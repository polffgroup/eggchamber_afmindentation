% This script is used to analyze stripe patterns in stiffness maps acquired
% via AFM by using their 2D Fourier transform. For each map, it calculates
% a profile of the absolute value of the Fourier components within a given 
% range of lengthscales as a function of their orientation. It also detects
% the dominant wavevector orientation and calculates an anisotropy index
% for each map.


%% Initialization
path='C:\thepath\';

lenprofile=200; %Number of points in the orthoradial profile
smoothspan=30; %Span of the moving average smoothing function
map = colormap(parula); close all;

CondLabel1='Ctrl'; %Condition label 1, typically control
CondLabel2='Cond2'; %Condition label 2
CondLabel3='Cond3'; %Condition label 3
MapSize_um=20; %Size of the map in µm
MapSize_px=256; %Size of the map in pixels

%Range of lengthscales to analyze in µm:
Lmin=0.5;
Lmax=1;

cd(path);
list=dir("*.tif");

profiles=NaN(length(list),3,lenprofile);
profiles_smooth=NaN(length(list),3,lenprofile);
anisotropy=NaN(length(list),3);
maxPos=NaN(length(list),3);
circle_coord=NaN(lenprofile,2);


%% Main processing loop
for fnum=1:length(list)
    AFMmap=imread([path '\' list(fnum).name]); %TIFF file to be analysed.
    imsize=size(AFMmap,1);
    trans_AFMmap=fft2(AFMmap);
    trans_AFMmap_shift=fftshift(trans_AFMmap); %Shift the zero-frequency
    %component to the center of the array
    
    %Remove the truly horizontal and truly vertical components from
    %the analysis:
    trans_AFMmap_shift_mod=trans_AFMmap_shift;
    trans_AFMmap_shift_mod(imsize/2,:)=NaN;
    trans_AFMmap_shift_mod(imsize/2+2,:)=NaN;
    trans_AFMmap_shift_mod(:,imsize/2)=NaN;
    trans_AFMmap_shift_mod(:,imsize/2+2)=NaN;
    trans_AFMmap_shift_mod(:,imsize/2+1)=NaN;
    trans_AFMmap_shift_mod(imsize/2+1,:)=NaN;

    kmax=MapSize_um/Lmin; %largest wavenumber considered
    kmin=MapSize_um/Lmax;
    if kmax>MapSize_px/2
        'Map resolution is too low! Please chose Lmin above '+string(2*MapSize_um/MapSize_px)
    end
    
    r_med=(kmax+kmin)/2; %median radius of the annulus
    circle_width=kmax-kmin+1; %width of the annulus
    
    %Calculate the coordinates of the points along the circle of radius
    %r_med:
    for i=1:lenprofile
        circle_coord(i,:)=[r_med*cos(i/lenprofile*2*pi)+imsize/2+1,r_med*sin(i/lenprofile*2*pi)+imsize/2+1];
    end
    
    %Coordinates of the extremities of the radial lines that span the width
    %of the annulus:
    [xEnds, yEnds]=OrtLineEndpoints(circle_coord(:,1),circle_coord(:,2), circle_width);
    
%     figure(2); hold on; plot(circle_coord(:,1),circle_coord(:,2));

    %Average the spectrum along the radial lines to create an orthoradial
    %profile:
    c=NaN(lenprofile,1);
    for i=1:lenprofile
       c(i)=mean(improfile(abs(trans_AFMmap_shift_mod), xEnds(:,i),yEnds(:,i)));
    end
    c_smooth=smooth(c,smoothspan);
    
%     figure(3) %For tests
%     hold on %For tests
%     plot((1:lenprofile)*2/lenprofile, c) %For tests
%     plot((1:lenprofile)*2/lenprofile, c_smooth) %For tests
%     title(list(fnum).name) %For tests
    
    if contains(list(fnum).name, CondLabel2)
        CondInd=2;
    elseif contains(list(fnum).name, CondLabel3)
        CondInd=3;
    else
        CondInd=1;
    end
    i=sum(isfinite(anisotropy(:,CondInd)))+1;

    
    %Find the angular position where the profile component is as much
    %higher as possible than the component at the orthogonal angular
    %position:
    cdiff=c_smooth(lenprofile/4:(lenprofile/2-1))-c_smooth(lenprofile/2:(lenprofile*3/4-1));

    [~,m]=max(abs(cdiff));
    if c_smooth(lenprofile/4+m-1)>c_smooth(lenprofile/2+m-1)
        maxPos(i,CondInd)=lenprofile/4+m-1; %Detected dominant orientation
        anisotropy(i,CondInd) = (c_smooth(maxPos(i,CondInd))-c_smooth(lenprofile/2+m-1)); %anisotropy index
    else
        maxPos(i,CondInd)=lenprofile/2+m-1; %Detected dominant orientation
        anisotropy(i,CondInd) = (c_smooth(maxPos(i,CondInd))-c_smooth(lenprofile/4+m-1)); %anisotropy index
    end
    
    profiles(i,CondInd,:)=c;
    profiles_smooth(i,CondInd,:)=c_smooth;

close all;
end


%% Plot the output data
%Boxplot of the anisotropy index:
figure
boxplot(anisotropy)
xticklabels([string(CondLabel1)+', N='+string(sum(isfinite(anisotropy(:,1))));string(CondLabel2)+', N='+string(sum(isfinite(anisotropy(:,2))));string(CondLabel3)+', N='+string(sum(isfinite(anisotropy(:,3))))]);
title('Anisotropy index')

%Histograms of the angular position of the dominant wavevectors:
figure
edges=0.05*(10:30);
h1 = histcounts(maxPos(isfinite(maxPos(:,1)),1)*2/lenprofile, edges, 'Normalization', 'probability');
h2 = histcounts(maxPos(isfinite(maxPos(:,2)),2)*2/lenprofile, edges, 'Normalization', 'probability');
h3 = histcounts(maxPos(isfinite(maxPos(:,3)),3)*2/lenprofile, edges, 'Normalization', 'probability');
bar(edges(1:end-1)+0.025,[h1; h2; h3]')
obj=findall(gcf,'-property','xdata');
obj(3).FaceColor='b';
obj(2).FaceColor='r';
obj(1).FaceColor='g';
legend({string(CondLabel1)+', N='+string(sum(isfinite(anisotropy(:,1)))) string(CondLabel2)+', N='+string(sum(isfinite(anisotropy(:,2)))) string(CondLabel3)+', N='+string(sum(isfinite(anisotropy(:,3))))}, 'Interpreter', 'none')
xticks(edges)
xlim([0.5 1.5])
labels=cell(21,1);
labels{1}="\pi/2"; labels{11}="\pi";labels{end}="\pi";
xticklabels(labels);
title('Orientation of the dominant wavevectors')
ylabel('Frequency of occurence')


% Average smoothed profiles:
a=nanmean(profiles_smooth(:,1,:),1);
b=nanmean(profiles_smooth(:,2,:),1);
c=nanmean(profiles_smooth(:,3,:),1);
a=a(:);
b=b(:);
c=c(:);
figure
hold on
plot((1:lenprofile)*2/lenprofile, a, 'b')
plot((1:lenprofile)*2/lenprofile, b, 'r')
plot((1:lenprofile)*2/lenprofile, c, 'g')
legend({string(CondLabel1)+', N='+string(sum(isfinite(anisotropy(:,1)))) string(CondLabel2)+', N='+string(sum(isfinite(anisotropy(:,2)))) string(CondLabel3)+', N='+string(sum(isfinite(anisotropy(:,3))))}, 'Interpreter', 'none')
ylim([0.6 1.6])
title('Average smoothed profiles along the annulus')
xticklabels({'0','\pi/2','\pi','3 \pi/2','2\pi'})

% Average raw profiles:
a=nanmean(profiles(:,1,:),1);
b=nanmean(profiles(:,2,:),1);
c=nanmean(profiles(:,3,:),1);
a=a(:);
b=b(:);
c=c(:);
figure
hold on
plot((1:lenprofile)*2/lenprofile, a, 'b')
plot((1:lenprofile)*2/lenprofile, b, 'r')
plot((1:lenprofile)*2/lenprofile, c, 'g')
legend({string(CondLabel1)+', N='+string(sum(isfinite(anisotropy(:,1)))) string(CondLabel2)+', N='+string(sum(isfinite(anisotropy(:,2)))) string(CondLabel3)+', N='+string(sum(isfinite(anisotropy(:,3))))}, 'Interpreter', 'none')
ylim([0.6 1.6])
title('Average raw profiles along the annulus')
xticklabels({'0','\pi/2','\pi','3 \pi/2','2\pi'})

%Boxplot of the distance to Pi of the dominant angular position:
figure
boxplot(abs(maxPos*2/lenprofile-1))
xticklabels([string(CondLabel1)+', N='+string(sum(isfinite(anisotropy(:,1))));string(CondLabel2)+', N='+string(sum(isfinite(anisotropy(:,2))));string(CondLabel3)+', N='+string(sum(isfinite(anisotropy(:,3))))]);
labels=cell(6,1);
labels{1}="0";labels{end}="\pi/2";
ylim([0 0.5])
yticks((0:5)*0.1)
yticklabels(labels); set(gca,"TickLabelInterpreter", 'tex')
title('Distance to \pi of the dominant wavevector orientation')


%% Additional resource
function [X_orth,Y_orth] = OrtLineEndpoints(X,Y,len)
% Calculates end points of orthogonal lines with the length len through 
% each point of the boundary curve (X,Y).

k = [-0.5,0.5];
X_orth = zeros(2,length(X));
Y_orth = zeros(2,length(Y));
for n = 1:length(X)
    switch n
        case 1          % starting point
            X_v = X(end); Y_v = Y(end);
            X_n = X(n+1); Y_n = Y(n+1);
        case length(X) % end point
            X_v = X(n-1); Y_v = Y(n-1);
            X_n = X(1); Y_n = Y(1);
        otherwise       % all other points
            X_v = X(n-1); Y_v = Y(n-1);
            X_n = X(n+1); Y_n = Y(n+1);
    end   
    D(1) = X_n - X_v; D(2) = Y_n - Y_v; % slope
    D = D/norm(D); D_neu(1) = D(2); D_neu(2) = -D(1); % orthogonal slope
    X_orth(:,n) =  X(n)+k*len*D_neu(1);
    Y_orth(:,n) =  Y(n)+k*len*D_neu(2);
end
end